require "multi_sms/version"
require 'configatron'
require 'rest-client'
require 'multi_json'
require 'json'
require "object"
require "multi_sms/util"


def gem_available?(gemname)
  Gem::Specification.find_all_by_name(gemname).size > 0
end

module MultiSms

  class ConfigError < RuntimeError; end
  # When the authentication can't be done
  class AuthError < RuntimeError; end
  # Raised when the API server isn't working
  class ServerError < RuntimeError; end
  # Generic exception when API gives a response we can't parse
  class BadResponse < RuntimeError; end
  # Generic exception calling an API the wrong way
  class BadRequest < RuntimeError; end
  # Raised when required paremeters are omitted
  class MissingParameter < RuntimeError; end

  class << self

    include MultiSms::Util

    attr_accessor :config

    # Responds with all the currently supported providers
    def providers
      %W(Clickatell Elk Twilio)
    end

    # Yields up a configuration object when given a block.
    # Without a block it just returns the configuration object.
    # Uses Configatron under the covers.
    #
    # Example:
    #   MultiSms.config do |c|
    #     c.default_provider = "Elk"
    #   end
    #
    #   MultiSms.config.foo # => :bar
    def config(&block)
      yield configatron.multi_sms if block_given?
      configatron.multi_sms
    end

    def send(country_code, parameters)
      raise ConfigError, "MultiSms needs a default provider configured." unless config.default_provider.instance_of?(String)
      verify_parameters(parameters, [:from, :message, :to])

      provider = get_provider country_code
      provider.send(parameters)
    end

    def logger
      @logger ||= begin
        log = Logger.new($stdout)
        log.level = Logger::INFO
        log
      end
    end

    def get_provider(country_code)
      name = config.providers[country_code.to_s]
      name = config.default_provider unless name.instance_of?(String)

      provider = "Providers::#{name}".split('::').inject(Object) {|o,c| o.const_get c}
      provider.new
    end

    # Wrapper for MultiSms.execute(:get)
    def get(base_url, path, parameters = {})
      execute(base_url, :get, path, parameters)
    end

    # Wrapper for MultiSms::execute(:post)
    def post(base_url, path, parameters = {})
      execute(base_url, :post, path, parameters)
    end

    # Wrapper around RestClient::RestClient.execute
    #
    # * Sets accept header to json
    # * Handles some exceptions
    #
    def execute(base_url, method, path, parameters, headers={ accept: :json}, &block)

      # Warn if the from string will be capped by the sms gateway
      if parameters[:from] && parameters[:from].match(/^(\w{11,})$/)
        warn "SMS 'from' value #{parameters[:from]} will be capped at 11 chars"
      end
      payload = {}.merge(parameters)
      url = base_url + path
      RestClient::Request.execute(:method => method, :url => url, :payload => payload, :headers => headers, &block)
    rescue RestClient::Unauthorized
      raise AuthError, "Authentication failed"
    rescue RestClient::InternalServerError
      raise ServerError, "Server error"
    rescue RestClient::Forbidden => e
      raise BadRequest, e.http_body
    end

    # Wrapper around MultiJson.load, symbolize names
    def parse_json(body)
      MultiJson.load(body, :symbolize_keys => true)
    rescue MultiJson::DecodeError
      raise BadResponse, "Can't parse JSON"
    end
  end

end

require "multi_sms/providers/sms_provider"
require "multi_sms/providers/elk"
require "multi_sms/providers/clickatell"
require "multi_sms/providers/twilio"