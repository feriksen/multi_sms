module Providers

  class Twilio < SmsProvider

    API_VERSION = "2010-04-01"
    BASE_DOMAIN = "api.twilio.com"

    def usable?
      config.twilio.account_sid.present? and config.twilio.auth_token.present?
    end


    # Send SMS
    # Required parameters
    #
    # * :from - Either the one of the allocated numbers or arbitrary alphanumeric string of at most 11 characters
    # * :to - Any phone number capable of receiving SMS
    # * :message - Any UTF-8 text Splitting and joining multi-part SMS messages are automatically handled by the API
    def send(parameters)
      base_url = "https://#{config.twilio.account_sid}:#{config.twilio.auth_token}@#{BASE_DOMAIN}/#{API_VERSION}"
      path = "/Accounts/#{config.twilio.account_sid}/SMS/Messages.json"

      response = RestClient.post base_url + path, :To => parameters[:to], :Body => parameters[:message], :From => parameters[:from]
      MultiSms.parse_json(response.body)
    rescue RestClient::Unauthorized
      raise AuthError, "Authentication failed"
    rescue RestClient::InternalServerError
      raise ServerError, "Server error"
    rescue RestClient::Forbidden => e
      raise BadRequest, e.http_body
    end
  end

end