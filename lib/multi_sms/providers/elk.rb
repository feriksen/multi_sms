module Providers

  class Elk < SmsProvider


    BASE_DOMAIN = 'api.46elks.com'
    API_VERSION = 'a1'

    def usable?
      config.elk.username.present? and config.elk.password.present?
    end

    def send(parameters)
      base_url = "https://#{config.elk.username}:#{config.elk.password}@#{BASE_DOMAIN}/#{API_VERSION}"
      path = "/SMS"
      { message: [] }
      response = MultiSms.post(base_url, path, parameters)

      MultiSms.parse_json(response.body)
    end

  end

end