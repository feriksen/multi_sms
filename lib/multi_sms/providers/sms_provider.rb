module Providers

  class SmsProvider

    def config
      MultiSms.config
    end

  private
    def method_missing(m, *args, &block)
      self.class.const_get(:API_VERSION)
    end
  end

end