module Providers

  class Clickatell < SmsProvider

    API_VERSION = ""
    BASE_DOMAIN = "api.clickatell.com"

    def usable?
      config.clickatell.apikey.present? and config.clickatell.username.present? and config.clickatell.password.present?
    end


    # Send SMS
    # Required parameters
    #
    # * :from - Either the one of the allocated numbers or arbitrary alphanumeric string of at most 11 characters
    # * :to - Any phone number capable of receiving SMS
    # * :message - Any UTF-8 text Splitting and joining multi-part SMS messages are automatically handled by the API
    def send(parameters)
      base_url = "http://#{BASE_DOMAIN}"
      path = "/http/sendmsg"
      url = base_url + path
      response = RestClient.post url, api_id: config.clickatell.apikey, user: config.clickatell.username, password: config.clickatell.password,  to: parameters[:to], text: parameters[:message]
    rescue RestClient::Unauthorized
      raise AuthError, "Authentication failed"
    rescue RestClient::InternalServerError
      raise ServerError, "Server error"
    rescue RestClient::Forbidden => e
      raise BadRequest, e.http_body
    end
  end

end