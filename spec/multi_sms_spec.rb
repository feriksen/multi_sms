require 'spec_helper'
describe MultiSms do

  context "with a default provider" do
    before do
      MultiSms.config do |config|
        config.providers = {"46" => "Elk", "47" => "Twilio"}
        config.default_provider = "Twilio"
      end
    end

    it "should raise and exception if #to is missing" do
      expect{MultiSms.send 46, from: "fake", message: "fake"}.to raise_error(MultiSms::MissingParameter)
    end

    it "should raise and exception if #from is missing" do
      expect{MultiSms.send 46, to: "fake", message: "fake"}.to raise_error(MultiSms::MissingParameter)
    end

    it "should raise and exception if #message is missing" do
      expect{MultiSms.send 46, to: "fake", from: "fake"}.to raise_error(MultiSms::MissingParameter)
    end

    it "should respond to providers" do
      MultiSms.providers.should eql ["Clickatell", "Elk", "Twilio"]
    end

    it "should get provider based on swedish country code" do
      provider = MultiSms.get_provider 46
      provider.should be_an_instance_of Providers::Elk
    end

    it "should get provider based on norweigan country code" do
      provider = MultiSms.get_provider 47
      provider.should be_an_instance_of Providers::Twilio
    end

    it "should not pick default_provider for unconfigured country codes" do
      provider = MultiSms.get_provider 45
      provider.should be_an_instance_of Providers::Twilio
    end

  end

  it "should raise and exception if no default provider is configured" do
    expect{MultiSms.send 46, to: "fake", from: "fake", message: "fake"}.to raise_error(MultiSms::ConfigError)
  end

end