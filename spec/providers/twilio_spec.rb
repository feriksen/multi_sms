require 'spec_helper'

describe Providers::Twilio do

  let(:provider) { Providers::Twilio.new }

  context "without authentication details" do
    it "should not be usable?" do
      provider.usable?.should be_false
    end
  end

  context "with authentication provided" do
    before do
      MultiSms.config do |config|
        config.twilio.account_sid = ENV['TWILIO_ACCOUNT_SID'] ||= "username"
        config.twilio.auth_token =  ENV['TWILIO_AUTH_TOKEN'] ||= "username"
      end
    end
    it "should be usable when configured" do
      provider.usable?.should be_true
    end

    it "should send sms as requested" do
      # MultiSms.logger.info provider.send(to: '+46730393200', message: 'testmessage', from: "+14155992671")
    end
  end

  it "should respond to version" do
    provider.version.should eql "2010-04-01"
  end

end
