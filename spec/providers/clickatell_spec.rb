require 'spec_helper'

describe Providers::Clickatell do

  let(:provider) { Providers::Clickatell.new }

  it "should not be usable unless configuration is complete" do
    expect(provider.usable?).to be_false
  end


  context "with authentication provided" do
    before do
      MultiSms.config do |config|
        config.clickatell.username = ENV['CLICKATELL_USERNAME'] ||= "username"
        config.clickatell.password = ENV['CLICKATELL_PASSWORD'] ||= "password"
        config.clickatell.apikey   = ENV['CLICKATELL_APIKEY'] ||= "apikey"
      end
    end

    it "should be usable when configured" do
      provider.usable?.should be_true
    end

    it "should send sms as requested" do
      # MultiSms.logger.info provider.send(to: '+46730393200', message: 'testmessage', from: "signlapps")
    end
  end

  it "should respond to version" do
    provider.version.should eql ""
  end

end
