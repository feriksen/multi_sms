require 'spec_helper'

describe Providers::Elk do
  let(:provider) { Providers::Elk.new }
  context "without authentication details" do
    it "should not be usable?" do
      provider.usable?.should be_false
    end
  end

  context "with authentication provided" do
    before do
      MultiSms.config do |config|
        config.elk.username = ENV['ELK_USERNAME'] ||= "username"
        config.elk.password = ENV['ELK_PASSWORD'] ||= "password"
      end
    end

    it "should be usable when configured" do
      provider.usable?.should be_true
    end

    it "should send sms as requested" do
      # MultiSms.logger.info provider.send(to: '+46730393200', message: 'testmessage', from: "signlapps")
    end
  end
  it "should respond to version" do
    provider.version.should eql "a1"
  end
end
