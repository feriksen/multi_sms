# -*- encoding: utf-8 -*-
require File.expand_path('../lib/multi_sms/version', __FILE__)

Gem::Specification.new do |gem|
  gem.authors       = ["Mikael Henriksson"]
  gem.email         = ["mikael@zoolutions.se"]
  gem.description   = %q{Gem for handling multiple SMS API's}
  gem.summary       = %q{Gem for handling multiple SMS API's}

  gem.homepage          = "http://github.com/signlapps/multi_sms"
  gem.license           = "MIT"
  gem.rubyforge_project = "multi_sms"
  # gem.executables   = ['']
  gem.files         = `git ls-files`.split("\n")
  gem.test_files    = `git ls-files -- test/*`.split("\n")
  gem.name          = "multi_sms"
  gem.require_paths = ["lib"]
  gem.version       = MultiSms::VERSION

  gem.add_runtime_dependency  'configatron'
  gem.add_runtime_dependency  'rest-client'
  gem.add_runtime_dependency  'multi_json'

  gem.add_development_dependency  'ruby_gntp'
  gem.add_development_dependency  'guard'
  gem.add_development_dependency  'guard-bundler'
  gem.add_development_dependency  'guard-rspec'
  gem.add_development_dependency  'guard-spork'
  gem.add_development_dependency  'spork', '> 0.9.0.rc'
  gem.add_development_dependency  'rspec'
  gem.add_development_dependency  'rake'
end
