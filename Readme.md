#multi_sms

Multi SMS is a simple gem built to handle multiple SMS providers with a unified api to use for sending SMS'es. It allows you to route outgoing SMS'es to different SMS providers based on the country code. It also centralizes the configuration of the actual provider gem's.

## Installation

Simply add multi_sms to your Gemfile:

```ruby
gem 'multi_sms'
```

and from command like just run the bundler:

```sh
bundle install
```

Or you CAN install the gem with the following command:

```sh
gem install multi_sms
```

## Configuration

1. 46elks
2. smsapi.pl - coming
3. clickatell - coming
4. twilio

Sample configuration for two providers and a default/fallback provider:

```ruby
# Clickatell
MultiSms.config do |config|
  config.clickatell.username = ENV['ELK_USERNAME']
  config.clickatell.password = ENV['ELK_PASSWORD']
  config.clickatell.api_id =  ENV['CLICKATELL_API_ID']
end

# 46Elks
MultiSms.config do |config|
  config.elk.username = ENV['ELK_USERNAME']
  config.elk.password = ENV['ELK_PASSWORD']
end

# Twilio
MultiSms.config do |config|
  config.twilio.account_sid = ENV['TWILIO_ACCOUNT_SID']
  config.twilio.auth_token =  ENV['TWILIO_AUTH_TOKEN']
end

# Multiple providers
MultiSms.config do |config|
  config.default_provider = "Elk"
  config.providers = { "46" => "Twilio", "47" => "Elk" }
end
```

A complete sample config:

```ruby
MultiSms.config do |config|
  config.clickatell.username = ENV['ELK_USERNAME']
  config.clickatell.password = ENV['ELK_PASSWORD']
  config.clickatell.api_id =  ENV['CLICKATELL_API_ID']

  config.elk.username = ENV['ELK_USERNAME']
  config.elk.password = ENV['ELK_PASSWORD']

  config.twilio.account_sid = ENV['TWILIO_ACCOUNT_SID']
  config.twilio.auth_token =  ENV['TWILIO_AUTH_TOKEN']

  config.default_provider = "Elk"
  config.providers = { "46" => "Twilio", "47" => "Elk", "1" => "Clickatell" }
end
```

## Usage

To actually use in your code, a simple sample is shown below:

```ruby
MultiSms.send(46, to: '+46730393200', from: '+14356778899', message: 'My SMS message')
```


## License

Copyright © 2012 Signl Apps Limited, http://www.signlapps.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE

